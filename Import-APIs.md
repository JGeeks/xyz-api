---
title: "Import APIs"
weight: 0
---
# Introduction
This document introduces how to define API documents, and how to setup cron script to import API documents from multiple git repos and keep documents up to date.

# Documents Definition
The definition is very simple and intuitive, each git repo includes single API documents set, all markdown files are at root, here are two sample repos as reference
```
https://gitlab.com/JGeeks/abc-api.git
https://gitlab.com/JGeeks/xyz-api.git
```

Each git repo must include one **_index.md** file to define what this API is
```
---
title: "XYZ API"
api_type: "Document Store API"
icon: "i/document-api.png"
description: "Description of the the API loremlore loareui ipsum doloaor sit amet, consectetur adipa dolscing elit, sed do eiusm lod tempor inici lorididunt ut labore."
weight: 0
---
```
* title - This API name.
* api_type - This API type name, must be in pre-defined types.
Note: they're not finalized, you can use one of following at current:
 * Authentication API
 * Event Manager API
 * IDM API
 * Document Store API
 * Notification Manager API
* icon - This API type icon, must be corresponding pre-defined api_type icons file path.
Note: they're not finalized, you can use one of following at current:
 * i/authentication-api.png
 * i/event-manager-api.png
 * i/idm-api.png
 * i/document-api.png
 * i/notification-api.png
* description - Description for this API.
* weight - Integer, define this API's order in /apis page list. more smaller more ahead.

Other markdown files can be direct from customer existing documents like their provided sample **Chronos.md** (Note: that sample defines all headers as 1st level #, and we only display 1st level archors in side menu), just need add following front matter at top
```
---
title: "Chronos"
weight: 0
---
```
* title - Title for this document, can be different with filename.
* weight - Integer, define this document's order in side menu, more smaller more ahead.


# Setup
Confirm you have git and running cron
```
git version
# For macOS
sudo cron status
# For Ubuntu Linux
service cron status
```
If not you need install and run
```
# For macOS
brew install git
sudo cron start

# For Ubuntu Linux
apt-get install git
apt-get install cron
service cron start
```

Config import-apis.conf
```
0 0 * * *
/home/z/Desktop/hugo-template/content/apis
https://gitlab.com/JGeeks/abc-api.git abc-api
https://gitlab.com/JGeeks/xyz-api.git xyz-api
```
* 1st line - Define cron schedule, you can refer https://en.wikipedia.org/wiki/Cron. The sample I set as everyday 0:00 for testing.
* 2nd line - Define where your hugo content apis folder path (without end slash).
* other lines -Define git url(without blank space) and target folder(without blank space, this folder will be in hugo content apis folder), seperated by one blank space.

Add or Update crontab, execute following command
```
# Must execute following command in this .sh file own folder, for this it is "cd hugo-template/extra"
sh import-apis.sh cron
```
Note: every time you edited 1st line of import-apis.conf, you need execute upper command to update crontab.


# Validation
See the demo.mp4 video as reference.

Install and run the hugo website following hugo-template/README.md
```
...
hugo server
```

Visit http://localhost:1313/apis , you can see the api list are empty.


Following upper **Setup** section to setup cron.  you can set the schedule more shorter such as every 1 minute just for testing
```
*/1 * * * *
```

After 1 minute, you can visit http://localhost:1313/apis/ again, now there're two new api in the list.

Clicking one api will go to its index page, there're sticky menu at left of the page; scrolling page will indicate archor in the menu; clicking archor will scroll the page; clicking page link will go to page; the menu will goto right-top when in mobile view.


Now we will test cron update feature, clone one your own git to some where
```
git clone git@gitlab.com:JGeeks/abc-api.git /tmp/abc
cd /tmp/abc
touch test.md
```

Edit and save test.md with following content
```
---
title: "Test Cron Update"
weight: 3
---
# This is a Test
Anything can put here.
```
Commit this change
```
git add .
git commit -m "test"
git push
```
After 1 minute, you can visit http://localhost:1313/apis/abc-api/ to see the new "Test Cron Update" document appears at left sticky menu.

Note: If you don't want test cron, you also can manually execute following command to test import and update
```
# Must execute following command in this .sh file own folder, for this it is "cd hugo-template/extra"
sh import-apis.sh import-apis.conf
```